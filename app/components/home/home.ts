import {Component} from 'angular2/core';
import { Model1 } from '../home/model1';
import {Router} from 'angular2/router';
import {ParseManager} from '../home/ParseManager';

interface Contractor {
  name: String;
}

@Component({
  selector: 'home',
  templateUrl: './components/home/home.html',
  styleUrls: ['./components/home/home.css']
})
export class HomeCmp {

public contractors = CONTRACTORS;

model = new Model1();

get diagnostic() { return JSON.stringify(this.model); }

constructor(public router: Router, public parseManager: ParseManager) {}

SaveToParse (name): boolean{
      this.parseManager.createCategory(name);
        }

// SavetoParse (diagnostic)
//     {
//     var CommentObject = Parse.Object.extend("CommentObject");
//     var user = new CommentObject;
//     user.set("args2", diagnostic);
//     user.save(null, {
//         success: function(user) {
//           alert('Save successful: ' + user.id);
//            },
//         error: function(user, error) {
//           alert('Save Failed:  ' + error.code + ' ' + error.message);
//             }
//           }
//     }


}

var CONTRACTORS: Contractor[] = [
  { "name": "GIRT" },
  { "name": "Designer" },
  { "name": "Intel" },
  { "name": "UPC" },
  { "name": "Sonas Recruitment" },
  { "name": "Hays Ireland" },
  { "name": "SSA" },
  { "name": "AJK" },
  { "name": "LIDL" },
  { "name": "ALDI" }
];